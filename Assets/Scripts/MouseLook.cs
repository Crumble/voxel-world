﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    private GameManager gm;
    private Vector2 mouseLook;
    private Vector2 smoothV;
    public float sensitivity = 5f;
    public float smoothing = 2f;

    private GameObject character;
    public PlayerController pc;

    public Vector2 lookVariable;
    private Vector2 lookSmoothing;

	// Use this for initialization
	void Start ()
	{
        
        character = this.transform.parent.gameObject;
        pc = transform.parent.GetComponent<PlayerController>();
	    lookSmoothing = new Vector2(sensitivity*smoothing, sensitivity*smoothing);

	    if (!pc.testingMode)
	    {
            gm = FindObjectOfType<GameManager>();
        }
	    
    }
	
	// Update is called once per frame
	void Update ()
	{
        lookVariable.x = Input.GetAxisRaw(pc.p1Input.LOOK_X_AXIS);
        lookVariable.y = Input.GetAxisRaw(pc.p1Input.LOOK_Y_AXIS);

        lookVariable = Vector2.Scale(lookVariable, lookSmoothing);
        smoothV.x = Mathf.Lerp(smoothV.x, lookVariable.x, 1f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, lookVariable.y, 1f / smoothing);
        mouseLook += smoothV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);

        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, Vector3.up);
    }
}
