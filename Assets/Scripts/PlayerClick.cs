﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerClick : MonoBehaviour {

    public World world;

    private LineRenderer laserLine;

    private WaitForSeconds clickDuration = new WaitForSeconds(1f);

    void Start()
    {
        laserLine = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetMouseButtonDown(0))
        {

            if (world.gameManager.Debug)
            {
                laserLine.enabled = true;
                laserLine.SetPosition(0, Camera.main.transform.position);
            }
            else
            {
                laserLine.enabled = false;
            }
            

            RaycastHit hitInfo;
            //Ray ray = Camera.main.ViewportPointToRay(Input.mousePosition);
            Vector3 rayOrigin = Camera.main.ViewportToWorldPoint(new Vector3(.5f, .5f, 0));


            if (Physics.Raycast(rayOrigin, Camera.main.transform.forward, out hitInfo, 100f))
            {
                if (world.gameManager.Debug)
                {
                    laserLine.SetPosition(1, hitInfo.point);
                }

                var newBlockPostion = hitInfo.point - (hitInfo.normal * 0.5f);
                

                int x = Mathf.RoundToInt(newBlockPostion.x);
                int y = Mathf.RoundToInt(newBlockPostion.y);
                int z = Mathf.RoundToInt(newBlockPostion.z);

                Block clickedBlock = world.GetBlock(x, y, z);

                if (clickedBlock.IsSolid(Block.Direction.North))
                {
                    //Debug.Log(clickedBlock.GetBlockId());
                    clickedBlock.PlayBreakSound();
                    world.SetBlock(x, y, z, new BlockAir());
                }
                else
                {
                    //is falce so we are clicking the block next to air?
                }
            }
            else
            {
                if (world.gameManager.Debug)
                {
                    laserLine.SetPosition(1, Camera.main.transform.forward * 100f);
                }
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (world.gameManager.Debug)
            {
                laserLine.enabled = true;
                laserLine.SetPosition(0, Camera.main.transform.position);
            }
            else
            {
                laserLine.enabled = false;

            }

            RaycastHit hitInfo;

            Vector3 rayOrigin = Camera.main.ViewportToWorldPoint(new Vector3(.5f, .5f, 0));

            if (Physics.Raycast(rayOrigin, Camera.main.transform.forward, out hitInfo, 100f))
            {

                if (world.gameManager.Debug)
                {
                    laserLine.SetPosition(1, hitInfo.point);
                }


                var newBlockPostion = hitInfo.point + (hitInfo.normal * 0.5f);

                int x = Mathf.RoundToInt(newBlockPostion.x);
                int y = Mathf.RoundToInt(newBlockPostion.y);
                int z = Mathf.RoundToInt(newBlockPostion.z);

                Block clickedBlock = world.GetBlock(x, y, z);

                if (clickedBlock.IsSolid(Block.Direction.North))
                {
                    
                }
                else
                {
                    Block newBlock = new BlockGrass();
                    world.SetBlock(x, y, z, newBlock);
                    newBlock.PlayPlaceSound();

                }


            }
        }

        //middle click spawn water?
        if (Input.GetMouseButtonDown(2))
        {
            if (world.gameManager.Debug)
            {
                laserLine.enabled = true;
                laserLine.SetPosition(0, Camera.main.transform.position);
            }
            else
            {
                laserLine.enabled = false;

            }

            RaycastHit hitInfo;

            Vector3 rayOrigin = Camera.main.ViewportToWorldPoint(new Vector3(.5f, .5f, 0));

            if (Physics.Raycast(rayOrigin, Camera.main.transform.forward, out hitInfo, 100f))
            {

                if (world.gameManager.Debug)
                {
                    laserLine.SetPosition(1, hitInfo.point);
                }


                var newBlockPostion = hitInfo.point + (hitInfo.normal * 0.5f);

                int x = Mathf.RoundToInt(newBlockPostion.x);
                int y = Mathf.RoundToInt(newBlockPostion.y);
                int z = Mathf.RoundToInt(newBlockPostion.z);


                
                Block bl = world.GetBlock(x, y, z);
                Block downBl = world.GetBlock(x, y - 1, z);

                //Check to see if we got a water block                                                                          
                if (bl.GetType().Equals(typeof(BlockAir)) && !downBl.GetType().Equals(typeof(BlockWater)))
                {
                    //set the new water block
                    world.SetBlock(x, y, z, new BlockWater());
                    //get the block we just placed
                    BlockWater newBlock = world.GetBlock(x, y, z) as BlockWater;
                    newBlock.SetWorld(world, x, y, z);
                    //and add it to register for update
                    newBlock.RegisterForUpdate();
                }

                


            }
        }

    }   
}
