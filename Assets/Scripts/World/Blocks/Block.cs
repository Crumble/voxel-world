﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Block { //each block subclasses this class
    
    
    protected World world; //reference to our world

    public int blockID = -1;
    public string blockName = "";

    //coordinates
    protected int blockX;
    protected int blockY;
    protected int blockZ;

	const float tileSize = 0.25f;
	const int SelectedSubmesh = 1;

	public int defaultSubmesh = 0;
	
	public int submesh = 0; //submesh is used to define which material will be used for each block
	private bool isSelected = false;
	public bool isWalkable = true;
	
	public bool changed = true;

    public AudioClip placeSound;
    public AudioClip breakSound;

    public struct Tile { //defines the tile in the texture
		public int x;
		public int y;
	}

	public Block() {
		submesh = defaultSubmesh;
	}

    public virtual void PlayBreakSound()
    {
        //do nothing since we have no sound.
    }

    public virtual void PlayPlaceSound()
    {
        //do nothing since we have no sound.
    }

    //Part3
    //We need to store our world so that we can access it later
    public void SetWorld(World w, int blockX,int blockY, int blockZ)
    {     
        world = w;
        this.blockX = blockX;
        this.blockY = blockY;
        this.blockZ = blockZ;
    }

    public virtual int GetBlockId()
    {
        return blockID;
    }

    public virtual string GetBlockName()
    {
        return blockName;
    }

    //The tick that we are going to override only through the water blocks
    public virtual void Tick(float deltaTime)
    {

    }

    //Put's the block on a list that we are going to call the Tick from
    public virtual void RegisterForUpdate()
    {
        if (world != null)
        {
            world.GetChunk(blockX, blockY, blockZ).TickBlocks.Add(this);
        }
    }

    //Removes it from the list
    public virtual void UnRegisterForUpdate()
    {
        if (world != null)
        {
            world.GetChunk(blockX, blockY, blockZ).TickBlocks.Remove(this);
        }
    }


	public virtual Tile TexturePosition(Direction direction) { 
        //the position of the texture in the atlas, 
        //direction is used to define the side of the block (e.g. grass is green on top but brown on the sides)
		Tile tile = new Tile();
		tile.x = 0;
		tile.y = 0;

		return tile;
	}

    public virtual Vector2[] FaceUVs(Direction direction) { //gets the UV's for the given side

		Vector2[] UVs = new Vector2[4];
		Tile tilePos = TexturePosition(direction);

		UVs[0] = new Vector2(tileSize * tilePos.x + tileSize, tileSize * tilePos.y);
		UVs[1] = new Vector2(tileSize * tilePos.x + tileSize, tileSize * tilePos.y + tileSize);
		UVs[2] = new Vector2(tileSize * tilePos.x, tileSize * tilePos.y + tileSize);
		UVs[3] = new Vector2(tileSize * tilePos.x, tileSize * tilePos.y);

		return UVs;
	}

	//Writes all the mesh data of the block
	public virtual MeshData BlockData(Chunk chunk, int x, int y, int z, MeshData meshData) {

		meshData.useRenderDataForCollision = true;

		//checks the neighbouring blocks if they are solid and if they are not, draws that face

		if(!chunk.GetBlock(x, y + 1, z).IsSolid(Direction.Down)) {
			meshData = FaceDataUp(chunk, x, y, z, meshData);
		}

		if(!chunk.GetBlock(x, y - 1, z).IsSolid(Direction.Up)) {
			meshData = FaceDataDown(chunk, x, y, z, meshData);
		}

		if (!chunk.GetBlock(x, y, z + 1).IsSolid(Direction.South))
		{
			meshData = FaceDataNorth(chunk, x, y, z, meshData);
		}
		
		if (!chunk.GetBlock(x, y, z - 1).IsSolid(Direction.North))
		{
			meshData = FaceDataSouth(chunk, x, y, z, meshData);
		}
		
		if (!chunk.GetBlock(x + 1, y, z).IsSolid(Direction.West))
		{
			meshData = FaceDataEast(chunk, x, y, z, meshData);
		}
		
		if (!chunk.GetBlock(x - 1, y, z).IsSolid(Direction.East))
		{
			meshData = FaceDataWest(chunk, x, y, z, meshData);
		}
		
		return meshData;
	}

	//Checks if a block is solid in a given direction
	public virtual bool IsSolid(Direction direction) {
		switch(direction) {
			case Direction.Down:
				return true;

			case Direction.East:
				return true;

			case Direction.North:
				return true;

			case Direction.South:
				return true;

			case Direction.Up:
				return true;

			case Direction.West:
				return true;
		}

		return false;
	}

	//Writes the up face
	protected virtual MeshData FaceDataUp(Chunk chunk, int x, int y, int z, MeshData meshData) {

		meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
		meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));

		meshData.AddQuadTriangles(submesh);

		meshData.uv.AddRange(FaceUVs(Direction.Up));

		return meshData;
	}

	protected virtual MeshData FaceDataDown(Chunk chunk, int x, int y, int z, MeshData meshData) {
		meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));
		meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));
		
		meshData.AddQuadTriangles(submesh);

		meshData.uv.AddRange(FaceUVs(Direction.Down));

		return meshData;
	}

	protected virtual MeshData FaceDataNorth(Chunk chunk, int x, int y, int z, MeshData meshData) {
		meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
		meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
		meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));
		
		meshData.AddQuadTriangles(submesh);

		meshData.uv.AddRange(FaceUVs(Direction.North));

		return meshData;
	}

	protected virtual MeshData FaceDataEast(Chunk chunk, int x, int y, int z, MeshData meshData) {
		meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));
		
		meshData.AddQuadTriangles(submesh);

		meshData.uv.AddRange(FaceUVs(Direction.East));

		return meshData;
	}

	protected virtual MeshData FaceDataWest(Chunk chunk, int x, int y, int z, MeshData meshData) {
		meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));
		meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
		meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
		meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
		
		meshData.AddQuadTriangles(submesh);

		meshData.uv.AddRange(FaceUVs(Direction.West));

		return meshData;
	}

	protected virtual MeshData FaceDataSouth(Chunk chunk, int x, int y, int z, MeshData meshData) {
		meshData.AddVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
		meshData.AddVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
		meshData.AddVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));
		
		meshData.AddQuadTriangles(submesh);

		meshData.uv.AddRange(FaceUVs(Direction.South));

		return meshData;
	}

	public enum Direction {
		North, East, South, West, Up, Down
	};

}
