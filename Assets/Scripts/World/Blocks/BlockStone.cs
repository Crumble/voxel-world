﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class BlockStone : Block
{

    public override int GetBlockId()
    {
        blockID = 3;
        return base.GetBlockId();
    }

    public override string GetBlockName()
    {
        blockName = "Stone";
        return blockName;
    }

    public override void PlayBreakSound()
    {
        string fileName = string.Format("Sounds/Blocks/{0}Break", GetBlockName());
        Debug.Log(fileName);
        this.breakSound = Resources.Load(fileName, typeof(AudioClip)) as AudioClip;
        this.world.gameManager.audioManager.PlaySound(this.breakSound);
    }

    public override void PlayPlaceSound()
    {
        string fileName = string.Format("Sounds/Blocks/{0}Place", GetBlockName());
        Debug.Log(fileName);
        this.placeSound = Resources.Load(fileName, typeof(AudioClip)) as AudioClip;
        this.world.gameManager.audioManager.PlaySound(this.placeSound);
    }

    //Same as block, with different textures
    public override Tile TexturePosition(Direction direction)
    {
        Tile tile = new Tile();
        switch (direction)
        {
            case Direction.Up:
                tile.x = 0;
                tile.y = 3;
                break;

            case Direction.Down:
                tile.x = 0;
                tile.y = 3;
                break;

            default:
                tile.x = 0;
                tile.y = 3;
                break;
        }

        return tile;

    }
}
