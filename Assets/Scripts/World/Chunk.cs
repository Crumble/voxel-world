﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]

public class Chunk : MonoBehaviour { //a collection of blocks

	public static int chunkSize = 16; //how many blocks in each direction

	public World world;
	public WorldPos pos;

	[HideInInspector]
	public bool update = false;

	public Block[ , , ] blocks = new Block[chunkSize, chunkSize, chunkSize]; //an array of all the blocks of the chunk

	private MeshFilter filter;
	private MeshCollider coll;

    //Part 3                                                                                                         ///---
    //This list holds every block that is going to run logic on update
    public List<Block> TickBlocks = new List<Block>();

	// Use this for initialization
	void Start () {
		filter = gameObject.GetComponent<MeshFilter>();
		coll = gameObject.GetComponent<MeshCollider>();
		
	}
	
	// Update is called once per frame
	void Update () {
		if(update) { //if update = true, a block of the chunk has changed and the chunk needs recalculation
			update = false;
			UpdateChunk();
		}

        //for every block into our List, run it's logic                                                     ///---
        for (int i = 0; i < TickBlocks.Count; i++)
        {
            TickBlocks[i].Tick(Time.deltaTime);

            //Keep in mind that this will run every frame so after a while you should stop doing that,
            //the pro to that is that it updates the blocks automatically
            //the con is performance, so here's where you need to do a little bit of optimization
        }
	}

	public Block GetBlock(int x, int y, int z) { //returns the block in the given position
		Block retBlock = null;
		if(InRange(x) && InRange(y) && InRange(z)) {//if the requested block is in this chunk, return it
			retBlock = blocks[x, y, z];
		} else { //otherwise, get it from the world
			retBlock = world.GetBlock(pos.x + x, pos.y + y, pos.z + z);
		}

		return retBlock;
	}

	public void SetBlock(int x, int y, int z, Block block) { //sets the block in the given position

        block.SetWorld(world, x,y,z);

		if(InRange(x) && InRange(y) && InRange(z)) { 
			blocks[x, y, z] = block;

		} else { 
			world.SetBlock(pos.x + x, pos.y + y, pos.z + z, block);
		}

	}

	void UpdateChunk() { //updates the chunk mesh data with the block meshdata
		MeshData meshData = new MeshData();

		for(int x=0; x<chunkSize; x++) 
        {
			for(int y=0; y<chunkSize; y++) 
            {
				for(int z=0; z<chunkSize; z++)
                {
					meshData = blocks[x, y, z].BlockData(this, x, y, z, meshData);
				}
			}
		}

		RenderMesh(meshData);
	}

	//sets the mesh to be rendered (uvs, materials, normals)
	void RenderMesh(MeshData meshData) {
		filter.mesh.Clear();

		filter.mesh.subMeshCount = meshData.triangles.Count;

		filter.mesh.vertices = meshData.vertices.ToArray();
		//filter.mesh.triangles = meshData.triangles.ToArray();
		for(int i=0; i<filter.mesh.subMeshCount; i++) {
			filter.mesh.SetTriangles(meshData.triangles[i].triangles.ToArray(), i);
		}

		filter.mesh.uv = meshData.uv.ToArray();
		filter.mesh.RecalculateNormals();

		coll.sharedMesh = null;
		Mesh mesh = new Mesh();
		mesh.vertices = meshData.colVertices.ToArray();
		//mesh.triangles = meshData.colTriangles.ToArray();

		mesh.SetTriangles(meshData.colTriangles.ToArray(), 0);

		mesh.RecalculateNormals();


		coll.sharedMesh = mesh;
	}


	public void SetBlockUnmodified() {
		foreach(Block block in blocks) {
			if(block != null) {
				block.changed = false;
			}
		}

	}
	
	//Checks if a block is within this chunk
	public static bool InRange(int index) {

		if(index < 0 || index >= chunkSize) {
			return false;
		}

		return true;
	}
}
