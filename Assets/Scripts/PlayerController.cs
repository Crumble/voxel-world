﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public bool testingMode = false;

    [System.Serializable]
    public class MoveSettings {
        public float speed = 10f;
        public float jumpVelocity = 300f;
        public float distToGrounded = 0.1f;
        public LayerMask ground;
    }


    [System.Serializable]
    public class InputSettings {
        public float inputDelay = 0.0f;
    }

    [System.Serializable]
    public class P1Input {
        public string JumpKey = "p1Jump";
        public string FORWARD_AXIS = "p1Vertical";
        public string STRAFFE_AXIS = "p1Horizontal";
        public string LOOK_X_AXIS = "p1LookX";
        public string LOOK_Y_AXIS = "p1LookY";
        public string PUSH_AXIS = "p1Fire1";
        public string PULL_AXIS = "p1Fire2";
    }

    

    public MoveSettings moveSetting = new MoveSettings();
    public InputSettings inputSettings = new InputSettings();
    public P1Input p1Input = new P1Input();

    

    private Rigidbody rBody;
    public int PlayerNumber = 1;

    private float forwardInput;
    float straffeInput;
    private float jumpInput;
    public bool jumped;

    public float PushForce = 50f;

    public bool isGrounded;

    private GameManager gm;

    public GameObject UI;

    public Transform flagHolder;

    // Use this for initialization
    void Start() {

        gameObject.name = "Player 1";
        Cursor.lockState = CursorLockMode.Locked;

        if(GetComponent<Rigidbody>()) {
            rBody = GetComponent<Rigidbody>();
        } else {
            Debug.LogError("No Rigid Body on Player");
        }
    }



    void GetInput() {
        //if we are player 1
        //use player 1 controls
        if(PlayerNumber == 1) {
            forwardInput = Input.GetAxis(p1Input.FORWARD_AXIS) * moveSetting.speed * Time.deltaTime;
            straffeInput = Input.GetAxis(p1Input.STRAFFE_AXIS) * moveSetting.speed * Time.deltaTime;
            jumpInput = Input.GetAxisRaw(p1Input.JumpKey);
        } else if(PlayerNumber == 2) {
           
        }

    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown("escape")) {
            Cursor.lockState = CursorLockMode.None;
        }

        //check if we are grounded.
        isGrounded = Physics.Raycast(transform.position, Vector3.down, moveSetting.distToGrounded, moveSetting.ground);

        if(isGrounded && jumpInput == 0 && jumped) {
            jumped = false;

        }

    }

    void FixedUpdate() {

        GetInput();

        //move player with input from GetINput()
        transform.Translate(straffeInput, 0, forwardInput);

        Jump();
    }


    void Jump() {
        if(jumpInput > 0 && isGrounded && !jumped) {
            //jump
            jumped = true;
            rBody.AddForce(0, moveSetting.jumpVelocity, 0, ForceMode.Impulse);
            
        }
        
    }
    
}
