Updated to Unity 2018.1.4f1

This is a minecraft clone type game made in Unity3d. Just wanted to see the process it would take and if i could get maximum performance.

![Video Here](https://i.imgur.com/ipj87p4.mp4) I made the sounds with my voice.

The game loads in a world when you click play of about 100+ chunks. Each Chunk is 16x16x16. Fps caps out in a desktop build at about 1000fps with vsync off.

The game is easy to extend, Just add a new block file in Scripts/World/Blocks.

Then add the block spawning height in World_Base.cs

You can run the demo scene by running the Scene 1 scene.

You can also place water tiles which spread out 8 tiles just like minecraft.

You can also save and load the world.

Move with WASD. 
Left click to remove block
Right click to place block
Middle Click to place water tile.

Let me know what you think!

Happy to hear any suggestions for improvements.

Feel free to use this as a start for your project. 